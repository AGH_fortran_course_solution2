# Copyright 2019 Wojciech Kosior

# This is free and unencumbered software released into the public domain.

# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.

# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

# For more information, please refer to <http://unlicense.org/>

ifneq ($(FC), ifort)
ifneq ($(FC), gfortran)

ifneq ($(shell which ifort 2>/dev/null),)
FC = ifort
endif

ifneq ($(shell which gfortran 2>/dev/null),)
FC = gfortran
endif

endif
endif


ifndef FFLAGS

ifeq ($(FC), gfortran)
FFLAGS = -std=f2008 -Wall -pedantic -fbounds-check -fimplicit-none \
	 -ffree-form -O2 -I/usr/include -lfftw3 -lm
endif

ifeq ($(FC), ifort)
FFLAGS = -std08  -module . -implicitnone -check bounds -O2 \
	 -I/usr/include -lfftw3
endif

endif

all : fourier

pngs : res/f1_dft.png res/f1_original.png res/f2_dft.png \
       res/f2_filtered.png res/f2_original.png

fourier : src/fourier.f90
	$(FC) $(FFLAGS) $^ -o $@

res/f1_original.txt : fourier
	./fourier f1 > res/f1_original.txt

res/f1_dft.txt : fourier
	./fourier f1 dft > res/f1_dft.txt

res/f2_original.txt : fourier
	./fourier f2 > res/f2_original.txt

res/f2_dft.txt : fourier
	./fourier f2 dft > res/f2_dft.txt

res/f2_filtered.txt : fourier
	./fourier f2 filtered > res/f2_filtered.txt

res/%.png : src/%.gnuplot res/%.txt
	gnuplot -c $<

clean :
	-rm fourier res/*.txt

.PHONY : all clean pngs
