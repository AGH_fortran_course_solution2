set terminal png size 1440, 900
set output 'res/f2_dft.png'
set logscale y
set xrange [0:150]
plot 'res/f2_dft.txt' title 'dft' with lines